from io import StringIO

from album.runner.api import setup


def export(output):
    import vtkplotlib as vpl
    from album.runner.api import get_args
    from pathlib import Path
    base_output_path = Path(get_args().output_directory)
    print("Exporting to %s.." % str(base_output_path.absolute()))

    shape = create_mesh_and_save(output, 30, base_output_path.joinpath("output.stl"))
    figure = vpl.figure()
    figure.render_size = (800, 600)
    vpl.mesh_plot(shape)
    vpl.show()


def create_mesh_and_save(data, threshold, file_name):
    import numpy as np
    from skimage import measure
    from stl import mesh
    verts, faces, normals, values = measure.marching_cubes(data, threshold)
    shape = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            shape.vectors[i][j] = verts[f[j], :]
    shape.save(file_name)
    return shape

def get_prog_key(prog, key):
    try:
        return prog[key]
    except KeyError:
        return None

def run():
    import moderngl
    import numpy as np
    from PIL import Image, ImageFilter

    ctx = moderngl.create_standalone_context()
    prog = ctx.program(
        vertex_shader='''
        #version 330
        in vec2 in_vert;
        out vec2 v_text;
        void main() {
            gl_Position = vec4(in_vert, 0.0, 1.0);
            v_text = in_vert;
        }
            ''',
        fragment_shader='''
#version 330
in vec2 v_text;
out vec4 f_color;
uniform float depth;
uniform float max_width;
uniform float max_height;
uniform float max_depth;

vec2 fold(vec2 p, float ang){
    vec2 n=vec2(cos(-ang),sin(-ang));
    p-=2.*min(0.,dot(p,n))*n;
    return p;
}

vec3 tri_fold(vec3 pt) {
    pt.xy = fold(pt.xy,0.947);
    pt.yz = fold(pt.yz,-21.338);
    pt.xy = fold(pt.xy,-0.191);
    pt.yz = fold(pt.yz,-3.214/3.164+sin(0.413*0.884)/-0.071);
    return pt;
}
vec3 tri_curve(vec3 pt) {
    for(int i=0;i<10;i++){
        pt*=1.592;
        pt.x-=8.672;
        pt=tri_fold(pt);
    }
    return pt;
}
float DE(vec3 p){
    //p *= 0.8643;
    p *= 3.0643;
    p.x+=2.168;
    p.y-=2.368;
    p.z+=0.268;
    p=tri_curve(p);
    return 5.528*(length( p*0.0028 ) - 0.052);
}

void main() {
    float x = (v_text.x * 0.5 + 0.5) * max_width;
    float y = (v_text.y * 0.5 + 0.5) * max_height;
    float z = (depth * 0.5 + 0.5) * max_depth;
    
    vec3 pt2 = vec3(x/(max_width), y/(max_height), z/(max_depth))*2.2-1.1;
    float index2Val2 = 255-255*DE(pt2);
    float val2 = max(0.0, index2Val2);
    float bounds = 2;
    if(x < bounds || x > max_width-bounds || y < bounds || y > max_height-bounds || z < bounds || z > max_depth-bounds) {
        f_color = vec4(0.0, 0.0, 0.0, 1.0);
    } else {
        f_color = vec4(0, max(0.0, min(val2/255., 1)), 0.0, 1.0);
    }    
}
''')

    depth = get_prog_key(prog, 'depth')
    max_width = get_prog_key(prog, 'max_width')
    max_height = get_prog_key(prog, 'max_height')
    max_depth = get_prog_key(prog, 'max_depth')

    vertices = np.array([-1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0])
    w = 512
    h = w
    d = w
    if max_width:
        max_width.value = w
    if max_height:
        max_height.value = h
    if max_depth:
        max_depth.value = d

    vbo = ctx.buffer(vertices.astype('f4'))
    vao = ctx.simple_vertex_array(prog, vbo, 'in_vert')
    data = np.zeros([w, h, d], dtype=np.float32)

    fbo = ctx.simple_framebuffer((w, h), components=4)
    fbo.use()

    for z in range(0, d):

        fbo.clear(0.0, 0.0, 0.0, 1.0)
        if depth:
            depth.value = (float(z)/float(d))*2 - 1

        vao.render(moderngl.TRIANGLE_STRIP)

        image = Image.frombytes("RGBA", fbo.size, fbo.read(components=4))
        image = image.transpose(Image.FLIP_TOP_BOTTOM)
        image = image.filter(ImageFilter.GaussianBlur(radius=1))
        image = image.filter(ImageFilter.GaussianBlur(radius=5))
        output_gauss = np.array(image)

        output = output_gauss[:, :, 1]

        data[:, :, z] = output

    fbo.release()

    export(data)


env_file = StringIO("""channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.8
  - moderngl=5.6.4
  - numpy=1.21.2
  - scikit-image=0.18.3
  - Pillow=8.4.0
  - numpy-stl=2.16.3
  - pip:
    - vtkplotlib==1.4.1
""")

setup(
    group="frauzufall",
    name="generate-3d-fractals",
    version="0.1.0-SNAPSHOT",
    title='Data generator app',
    description='This solution generates 3D mesh data based on 3D Kaleidoscopic Fractals.',
    album_api_version="0.3.1",
    authors=['Deborah Schmidt'],
    run=run,
    cite=[
        {
            'text': 'Szabolcs Dombi; ModernGL, high performance python bindings for OpenGL 3.3+.',
            'url': 'https://github.com/moderngl/moderngl'
        },
        {
            'text': 'Roy Wiggins; 3D Kaleidoscopic Fractals: Folding the Koch Snowflake.',
            'url': 'http://roy.red/posts/folding-the-koch-snowflake/'
        }
    ],
    args=[{
        "name": "output_directory",
        "type": "string",
        "required": True,
        "description": "The solution will generate mesh data and store it in this directory."
    }],
    dependencies={'environment_file': env_file}
)
